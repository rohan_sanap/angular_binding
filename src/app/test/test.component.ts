import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  template: `
  <input [(ngModel)] =name type ="text">
  {{name}}`,
  

  styles: []
})
export class TestComponent implements OnInit {
  constructor() { }

  public name = ""
  ngOnInit(): void {
  }

  
}


//Inside templet 
  // <input type= "text" value ="ROHAN">
  //  <input bind-disabled  = "isDisabled" type= "text" value ="ROHAN">
  // <h2 class ="text-success">Normal binding</h2>
  // <h2 [class] ="dangerText">Class binding</h2>
  // <h2 [class.text-danger] ="hasError">Single conditional</h2> 
  // <h2 [ngClass] = "messageClasses">MULTIPLE CONDITIONAL WITH THE HELP OF NG CLASS</h2>
  // <h2 [style.color] = "'yellow'">Style binding </h2> 
  // <h2 [style.color] = "hasError ? 'red' : 'green'">Style binding with condition </h2> 
  // <h2 [style.color] = "highLitedColor"> Highlighted colors </h2>
  // <h2 [ngStyle] = "fontStyle">MULTIPLE CONDITIONAL WITH THE HELP OF NG STYLE </h2>
  // <button (click) = "onClick()">Greet</button>
  // <button (click) = "eventMessage= 'Welcome Rohan ' ">Greet</button>
  // {{eventMessage}}

  // <h2>Welcome {{name}}</h2>
  // <input #myInput type="text">
  // <button (click)="logMessage(myInput)">Display</button>
  

// Inside style[] =
//   .text-success{
//     color:green;
//   }
//   .text-danger{
//     color :red;
//   }
//   .text-special{
//     font-style:italic;
//   }
//   

// Inside Class

//
// public isDisabled = false
// public dangerText = "text-danger" 
// public hasError = false
// public isSpecial = true
// public highLitedColor = "orange"
// public isItalic = true
// public eventMessage = ""

// public messageClasses = {
//   "text-success" : !this.hasError,
//   "text-danger" : this.hasError,
//   "text-special" : this.isSpecial
// }

// public fontStyle = {
//   color : "blue",
//   fontStyle : "italic"
// }



// greeting(){
//   return "Hello " + this.name;
// }
// onClick(){
//   console.log('Welcome to Onclick event binding')
//   this.eventMessage ="Welcome to to demo of Event binding"
// }

// logMessage(value:any){
//   console.log(value)
// }

